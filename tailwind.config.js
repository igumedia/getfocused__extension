module.exports = {
  purge: [],
  purge: {
    enabled: true,
    content: [
      './static/**/*.html',
      './static/**/*.js',
      './src/views/**/*.js',
      './src_option/**/*.js',
      './src_reminder/**/*.js',
      './src_snooze_counter_popup/**/*.js'
    ]
  },
  theme: {
    filter: { // defaults to {}
      'none': 'none',
      'grayscale': 'grayscale(1)',
      'invert': 'invert(1)',
      'sepia': 'sepia(1)',
    },
    backdropFilter: { // defaults to {}
      'none': 'none',
      'blur': 'blur(20px)',
    },
    fontFamily: {
      'body': ['Inter', 'sans-serif'],
      'sans': 'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"'
    },
    extend: {},
  },
  variants: {
    backgroundColor: ['responsive','odd', 'hover', 'focus'],
    textColor: ['responsive', 'hover', 'focus', 'active'],
    borderColor: ['responsive', 'hover', 'focus', 'active'],
    filter: ['responsive'], // defaults to ['responsive']
    backdropFilter: ['responsive'], // defaults to ['responsive']
  },
  plugins: [
    require('tailwindcss-filters'),
  ],
}
