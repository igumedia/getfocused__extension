let snoozeCounterPopup = document.createElement('iframe');
snoozeCounterPopup.src = chrome.extension.getURL("snooze-counter-popup.html");
snoozeCounterPopup.className = 'getFocused-snoozeCounter-popup';
snoozeCounterPopup.frameBorder = 0;
snoozeCounterPopup.id = 'getFocused__snoozeCounter-popup';
snoozeCounterPopup.height = '100%';

// functions
let Action = {
    showSnoozeCounterPopup: function(){ // snooze counter function
        document.body.appendChild(snoozeCounterPopup);
    },
    dismissSnoozeCounterPopup: function(){  // dismiss snooze counter function
        let snoozeCounterPopupelement = document.getElementById('getFocused__snoozeCounter-popup');
        snoozeCounterPopupelement.parentNode.removeChild(snoozeCounterPopupelement);
    },
    countSnoozeCounter: function(type){
        chrome.storage.local.get(['snoozeCounter_totalCount', 'snoozeCounter_totalDuration'], function(result){
            console.log(result)
        })
    }
}

// post message browser listener
window.addEventListener("message", function(event) {

    // handler for snooze coutner height message
    if(event.data.snoozeCounter_height){
        // change snooze counter height
        snoozeCounterPopup.height = event.data.snoozeCounter_height + 'px'
    }

  }, false);

// chrome listener
chrome.runtime.onMessage.addListener(function(message){

        // --- handler for message that tell to show snozze counter
        if (message.snoozeCounterPopup_ShowIframe) {
            Action.showSnoozeCounterPopup()
        } 

        // --- handler for message that tell to remove iframe
        if(message.snoozeCounterPopup_removeIframe){
            Action.dismissSnoozeCounterPopup()
        }




})