chrome.runtime.onInstalled.addListener(function () {
  console.log('getFocused is running');
  var reminderPopupSnoozeDuration = [1, 1, 10, 60];
  var distractingWebsites = ["facebook.com", "twitter.com", "pinterest.com", "tumblr.com", "instagram.com", "flickr.com", "meetup.com", "ask.fm", "netflix.com", "youtube.com", "reddit.com", "hulu.com", "imgur.com", "vimeo.com", "ted.com", "blogger.com", "imdb.com", "deviantart.com", "break.com", "collegehumor.com", "funnyordie.com", "liveleak.com", "twitch.tv", "soundcloud.com", "theonion.com", "cracked.com", "tmz.com", "vice.com", "rottentomatoes.com", "ebay.com", "craigslist.org", "etsy.com", "amazon.com", "bbc.com", "forbes.com", "economist.com", "nbcnews.com", "cnn.com", "foxnews.com", "msnbc.com", "huffingtonpost.com", "businessinsider.com", "buzzfeed.com", "nytimes.com", "bloomberg.com", "usatoday.com", "washingtonpost.com", "theguardian.com", "npr.org", "wsj.com", "time.com", "cnet.com", "cnbc.com", "bleacherreport.com", "espn.com", "foxsports.com", "cbssports.com", "mlb.com", "nfl.com", "nhl.com", "nba.com", "techcrunch.com", "mashable.com", "engadget.com", "gizmodo.com", "gigaom.com", "boingboing.net", "wired.com", "theverge.com", "lifehacker.com", "readwrite.com", "recode.net", "arstechnica.com", "macrumors.com", "tomshardware.com", "thenextweb.com", "9to5mac.com", "zdnet.com", "venturebeat.com", "slashdot.org", "digg.com", "techmeme.com", "fark.com", "popurls.com", "metafilter.com"]; // store default value for distracting websites if none in localstorage

  chrome.storage.local.get('distractingWebsites', function (result) {
    if (!result.distractingWebsites) {
      chrome.storage.local.set({
        distractingWebsites: distractingWebsites
      });
    }
  });

  // store default value for reminder popup snooze duration websites if none found in localstorage
  chrome.storage.local.get('reminderPopupSnoozeDuration', function (result) {
    if (!result.reminderPopupSnoozeDuration) {
      chrome.storage.local.set({
        reminderPopupSnoozeDuration: reminderPopupSnoozeDuration
      });
    }
  });

  // setup state
  chrome.storage.local.set({
    reminderPopup_snoozedWebsites: {},
    snoozeCounter_container: { 
      snoozeCounter_totalCount: 0,
      snoozeCounter_totalDuration: {
        snoozeUntil: null,
        snoozeTotalDuration: null
      }
    } 
  });

});

chrome.runtime.onStartup.addListener(function () {
    // empty snoozed websites when browser start
    chrome.storage.local.set({
      // empty snoozed website list when start up
      reminderPopup_snoozedWebsites: {}, 
      // empty snooze counter when start up
      snoozeCounter_container: { 
        snoozeCounter_totalCount: 0,
        snoozeCounter_totalDuration: {
          snoozeUntil: null,
          snoozeTotalDuration: null
        }
      } 
    });

});


