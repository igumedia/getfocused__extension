var m = require("mithril")
var taskModel = require('../model/Task').Task
var dateformat = require("dateformat")
var prettyMilliseconds = require('pretty-ms');

module.exports = {
    oninit: function(){
        taskModel.initialLoad()
    },
    view: function(){
        return m('div.text-base font-body',{ 
            style: 'width:360px;'
        },[
            m('div.p-2 bg-gray-100 flex flex-row justify-between items-baseline', [
                m('div.bg-gray-100 font-sans p-1 text-xl text-teal-400 border-b-2 border-teal-400 inline-block rounded-lg cursor-pointer', 'Get Focused'),
                m('a.text-2xl text-gray-500 hover:text-teal-500 focus:outline-none hover:cursor-pointer',{
                    href: taskModel.optionURL,
                    title: 'getFocused setting',
                    target: '_blank'
                },'⚙')
            ]),
            m('form.bg-gray-100 p-2',{
                onsubmit: function(e){
                    e.preventDefault()
                    taskModel.submitTask()
                }
            }, [
                m('.flex.flex-col', [
                    m('textarea.rounded border border-gray-400 p-2 text-base focus:outline-none hover:border-teal-300 focus:border-teal-400', {
                        id: 'taskInput',
                        placeholder: "What is your goal for today?",
                        value: taskModel.taskInput,
                        required: true,
                        oninput: function(e){                    
                            taskModel.setTaskInput(e.target.value)
                        },
                        onkeydown: function(e){
                            if (e.which == 13) {
                                e.preventDefault()
                                taskModel.submitTask()
                            }
     
                        }
                    }),
                    m('button.bg-teal-400 hover:bg-teal-500 focus:outline-none  focus:bg-teal-500 block text-white mt-2 rounded-full py-2 px-3 self-end', 'Add task')
                ])
            ]),
            m('div.p-2', {
               class: taskModel.taskBag.length ? null : 'hidden' 
            },[
                m('nav.flex flex-row justify-center text-gray-700 text-base', [
                    m('button.p-2 focus:outline-none mr-4 border-b-2 hover:text-teal-500 hover:border-teal-500',{
                        class: taskModel.taskView === 'all' ? 'text-teal-500 font-medium border-teal-500' : null,
                        onclick: function(){
                            taskModel.setTaskView('all')
                        }
                    },'All Tasks'),
                    m('button.p-2 focus:outline-none mr-4 border-b-2 hover:text-teal-500 hover:border-teal-500', {
                        class: taskModel.taskView === false ? 'text-teal-500 font-medium border-teal-500' : null,
                        onclick: function(){
                            taskModel.setTaskView(false)
                        }
                    }, 'To-dos'),
                    m('button.p-2 focus:outline-none mr-4 border-b-2 hover:text-teal-500 hover:border-teal-500', {
                        class: taskModel.taskView === true ? 'text-teal-500 font-medium border-teal-500' : null,
                        onclick: function(){
                            taskModel.setTaskView(true)
                        }
                    }, 'Done')
                ]),
                m('div.my-5 text-base font-text text-gray-800', [
                    m('ul.px-4 leading-relaxed', 
                        taskModel.taskBag.map(function(task){
                            if(taskModel.taskView === task.completed || taskModel.taskView === 'all' ){
                            return m('li.text-base p-2 mb-4 font-body font-light flex flex-row items-baseline odd:bg-gray-100 rounded hover:bg-teal-100', {
                                key: task.taskId,
                            }, [
                                    m('input', {
                                        type:'checkbox',
                                        checked: task.completed === true ? true : false,
                                        onchange: function(){
                                            taskModel.setCompleted(task)
                                        }
                                    }),
                                    m('div.ml-2 flex-grow cursor-pointer',{
                                        class: task.completed === true? 'font-bold text-teal-600' : null,
                                        onclick: function(){
                                            taskModel.setExpandDetails(task)
                                        }
                                    }, task.taskDesc, [
                                        m('div.text-sm p-4 leading-relaxed text-gray-800 font-light', {
                                            class: task.expandDetails ? null : 'hidden'
                                        }, [
                                            m('div.mb-1', 'Created at: ' + dateformat(task.createdAt, 'dd mmm yy, hh:MMTT') ),
                                            m('div.mb-1', task.completedAt ? 'Completed at: ' + dateformat(task.completedAt, 'dd mmm yy, hh:MMTT') : 'Completed at: Pending'),
                                            m('div.mb-4', task.TTC ? 'Time to completion: ' + prettyMilliseconds(task.TTC)  : 'Time to completion: On Progress'),                                   
                                            m('div.hover:bg-red-400 hover:text-white inline-block rounded p-1',{
                                                onclick: function(){
                                                    taskModel.removeTask(task.taskId)
                                                }
                                            },'[x] Remove task')                                    
                                        ])
                                    ]),
                                    task.completed === true ? m('div.font-bold text-teal-600 text-2xl', '✔') : null
                                ])
                            
                            }
                            else{
                                return m.fragment({key: task.taskId}, [])
                            }

                        }) 
                        // m('li.text-base p-2 mb-4 font-body font-light flex flex-row items-baseline cursor-pointer odd:bg-gray-100 rounded hover:bg-teal-100', [
                        //     m('input', {type:'checkbox'}),
                        //     m('div.ml-2 font-bold text-teal-600 flex-grow', 'Create an UI element banana una manana panana', [
                        //         m('div.text-sm p-4 leading-relaxed text-gray-800 font-light', [
                        //             m('div.mb-1', 'Created at: 12 Jul 20, 09:44PM'),
                        //             m('div.mb-1', 'Completed at: Pending'),
                        //             m('div.mb-4', 'Time to completion: 1d 6h 9m 15.5s'),                                   
                        //             m('div', '[x] Remove task')                                    
                        //         ])
                        //     ]),
                        //     m('div.font-bold text-teal-600 text-2xl', '✔')
                        // ]),
                        // m('li.text-base p-2 mb-4 font-body font-light flex flex-row items-baseline cursor-pointer odd:bg-gray-100 rounded hover:bg-teal-100', [
                        //     m('input', {type:'checkbox'}),
                        //     m('div.ml-2 font-bold text-teal-600 flex-grow', 'Create an UI element banana una manana panana', [
                        //         m('div.text-sm p-4 leading-relaxed text-gray-800 font-light', [
                        //             m('div.mb-1', 'Created at: 12 Jul 20, 09:44PM'),
                        //             m('div.mb-1', 'Completed at: Pending'),
                        //             m('div.mb-4', 'Time to completion: 1d 6h 9m 15.5s'),                                   
                        //             m('div', '[x] Remove task')                                    
                        //         ])
                        //     ]),
                        //     m('div.font-bold text-teal-600 text-2xl', '✔')
                        // ]),
                        // m('li.text-base p-2 mb-4 font-body font-light flex flex-row items-baseline cursor-pointer odd:bg-gray-100 rounded hover:bg-teal-100', [
                        //     m('input', {type:'checkbox'}),
                        //     m('div.ml-2 font-bold text-teal-600 flex-grow', 'Create an UI element banana una manana panana', [
                        //         m('div.text-sm p-4 leading-relaxed text-gray-800 font-light', [
                        //             m('div.mb-1', 'Created at: 12 Jul 20, 09:44PM'),
                        //             m('div.mb-1', 'Completed at: Pending'),
                        //             m('div.mb-4', 'Time to completion: 1d 6h 9m 15.5s'),                                   
                        //             m('div', '[x] Remove task')                                    
                        //         ])
                        //     ]),
                        //     m('div.font-bold text-teal-600 text-2xl', '✔')
                        // ]),
                        // m('li.text-base p-2 mb-4 font-body font-light flex flex-row items-baseline cursor-pointer odd:bg-gray-100 rounded hover:bg-teal-100', [
                        //     m('input', {type:'checkbox'}),
                        //     m('div.ml-2 font-bold text-teal-600 flex-grow', 'Create an UI element banana una manana panana', [
                        //         m('div.text-sm p-4 leading-relaxed text-gray-800 font-light', [
                        //             m('div.mb-1', 'Created at: 12 Jul 20, 09:44PM'),
                        //             m('div.mb-1', 'Completed at: Pending'),
                        //             m('div.mb-4', 'Time to completion: 1d 6h 9m 15.5s'),                                   
                        //             m('div', '[x] Remove task')                                    
                        //         ])
                        //     ]),
                        //     m('div.font-bold text-teal-600 text-2xl', '✔')
                        // ]),
                    )
                ]),
                // m('div.p-2 text-center text-teal-500', [
                //     m('div.hover:text-teal-600', '❤	 Keep me caffeinated, buy me a coffee')
                // ])
            ]),
        ])
    }
}