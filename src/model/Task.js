var m = require('mithril')
var uniqId = require('uniqid')

var Task = {
    optionURL: '',
    taskBag: [],
    taskInput: '',
    taskView: 'all',
    setTaskInput: function(value){
        Task.taskInput = value
    },
    setExpandDetails: function(task){
        task.expandDetails = !task.expandDetails
    },
    setTaskView: function(value){
        Task.taskView = value
    },
    storeToLocal: function(){
    
        chrome.storage.local.set({taskBag: Task.taskBag});
    
    },
    initialLoad: function(){
        Task.optionURL = chrome.runtime.getURL('/option.html')

        chrome.storage.local.get(['taskBag'], function(result) {
            const taskBag = result.taskBag
           
            if(taskBag.length > 0 ){
                Task.taskBag = taskBag
            }else{
                Task.taskBag = []
            }
            m.redraw()
            document.getElementById('taskInput').focus()
        });


    },
    removeTask: function(taskId){
        for( var i = 0; i < Task.taskBag.length; i++){ 
            if ( Task.taskBag[i].taskId === taskId) {
                Task.taskBag.splice(i, 1); 
            }
         }

         Task.storeToLocal()
    },
    setCompleted: function(task){
        var status = !task.completed
        task.completed = status

        if(status === true){
            var completedDate = new Date().toISOString() 
            task.completed = true
            task.completedAt = completedDate
            task.TTC = Math.abs(new Date(completedDate) - new Date(task.createdAt))

        }else{
            task.completed = false
            task.completedAt = false
            task.TTC = false
        }

        Task.storeToLocal()
    },
    submitTask: function(){
        
        
        if(Task.taskInput.trim()){
            console.log('aaa')
            var taskId = uniqId('task_')
            Task.taskBag.unshift({
                taskId : taskId,
                expandDetails: false,
                completed: false,
                createdAt: new Date().toISOString(),
                completedAt: false,
                TTC: false,
                taskDesc: Task.taskInput,
            })
    
            Task.storeToLocal()
        }

        document.getElementById('taskInput').focus()
        

        Task.taskInput = ''

    }
}

module.exports = {
    Task
}