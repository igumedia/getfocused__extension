chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  // ----- handler reminder pop up launch, check weither the url is distracting or unlocked
  if (request.reminderPopup_checkUrl) {
    chrome.storage.local.get(['distractingWebsites', 'reminderPopup_snoozedWebsites'], function (result) {
      // list of distracting websites
      var listDistractingWebsites = result.distractingWebsites; // get current current full URL

      var reminderPopup_currentUrl = new URL(sender.tab.url); // parse the domain only

      var reminderPopup_parsedUrl = psl.parse(reminderPopup_currentUrl.host); // if the URL is not distracting or unlocked for the session, then don't show reminder popup. Otherwise show it

      if ( // is visited URL on distracting website list ?
      listDistractingWebsites.indexOf(reminderPopup_parsedUrl.domain) != -1 && // is visited URL Unlocked for the current session?
      sessionStorage.getItem(reminderPopup_currentUrl.host) !== '1' && // is visited URL on snoozed list ?
      result.reminderPopup_snoozedWebsites[reminderPopup_parsedUrl.domain] != true) {
        // // send message instructing to show the reminder popup
        chrome.tabs.sendMessage(sender.tab.id, {
          reminderPopup_show: true
        });
      }
    });
  } // ----- handler for closing tab from reminder popup


  if (request.reminderPopup_closeTab) {
    chrome.tabs.query({
      active: true,
      currentWindow: true
    }, function (tabs) {
      chrome.tabs.remove([tabs[0].id]);
    });
  } // ----- handler for snoozing tabs


  if (request.reminderPopup_snooze) {
    // get current current full URL
    var reminderPopup_currentUrl = new URL(sender.tab.url); // parse only the domain

    var reminderPopup_parsedUrl = psl.parse(reminderPopup_currentUrl.host); // send message to all tabs with selected domain that tell the snpoze duration is over

    chrome.tabs.query({
      url: '*://*.' + reminderPopup_parsedUrl.domain + '/*'
    }, function (result) {
      for (var i = 0; i < result.length; i++) {
        var element = result[i];
        chrome.tabs.sendMessage(element.id, {
          reminderPopup_hide: true
        });
      }
    }); // add snoozed URL to snooze website list

    chrome.storage.local.get('reminderPopup_snoozedWebsites', function (result) {
      var snoozedWebsites = result.reminderPopup_snoozedWebsites; // give snoozed websites value to true

      snoozedWebsites[reminderPopup_parsedUrl.domain] = true;
      chrome.storage.local.set({
        'reminderPopup_snoozedWebsites': snoozedWebsites
      });
    }); // get snooze duration from local storage

    chrome.storage.local.get('reminderPopupSnoozeDuration', function (result) {
      // get specific snooze value
      var snoozeDuration = result.reminderPopupSnoozeDuration[request.snooze_type]; // create alarm according to duration
      // format example : reminderPopup||twitter.com||213

      chrome.alarms.create('reminderPopup||' + reminderPopup_parsedUrl.domain + '||' + sender.tab.id, {
        // when: Date.now() + 10000 ---> for development only
        delayInMinutes: snoozeDuration
      });
    });
  }
  /* unused
  // --- handler for unlock session
  if (request.reminderPopup_unlockSession) {
    // get tab URL
    chrome.tabs.query({
      active: true,
      currentWindow: true
    }, function (tabs) {
      // get unlocked URL
      var unlockedSessiondUrl = new URL(tabs[0].url); // store unlocked URL in session storage
         window.sessionStorage.setItem(unlockedSessiondUrl.host, '1'); // send message to hide reminder popup iframe            
         chrome.tabs.sendMessage(tabs[0].id, {
        reminderPopup_hide: true
      });
    });
  }
  */

}); // listening to alarm

chrome.alarms.onAlarm.addListener(function (alarm) {
  // parse alarm name
  var parsedAlarmName = alarm.name.split('||'); // get alarm type

  var alarmType = parsedAlarmName[0]; // index 0: alaram type | index 1: domain name | index 2: tab id
  // alarm when snooze duration is ended

  if (alarmType === "reminderPopup") {
    // remove from snoozed list
    chrome.storage.local.get('reminderPopup_snoozedWebsites', function (result) {
      var snoozedWebsites = result.reminderPopup_snoozedWebsites; // set snoozed status to false (off)

      snoozedWebsites[parsedAlarmName[1]] = false; // remove website from snoozed list

      chrome.storage.local.set({
        'reminderPopup_snoozedWebsites': snoozedWebsites
      }, function () {
        // send message to all tabs that the selected domain's snpoze duration is over, so show the reminder popup
        chrome.tabs.query({
          url: '*://*.' + parsedAlarmName[1] + '/*'
        }, function (result) {
          for (var i = 0; i < result.length; i++) {
            var element = result[i];
            chrome.tabs.sendMessage(element.id, {
              reminderPopup_snoozeOver: true
            });
          }
        });
      });
    });
  }
});