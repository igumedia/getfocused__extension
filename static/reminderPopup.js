var reminderPopupFrame = document.createElement('iframe');
reminderPopupFrame.src = chrome.extension.getURL("reminder.html");
reminderPopupFrame.className = 'getFocused-reminder-popup';
reminderPopupFrame.frameBorder = 0;
reminderPopupFrame.id = 'getFocused__reminder-popup'; // when a page is loaded, check weither the visited website is distracting or not

chrome.runtime.sendMessage({
  reminderPopup_checkUrl: true,
  reminderPopup_currentUrl: window.location.host
}); // message listener

chrome.runtime.onMessage.addListener(function (message) {
  // --- message that tell to hide reminder pop up
  if (message.reminderPopup_hide) {
    // remove iframe from the page
    var getFocused__element = document.getElementById('getFocused__reminder-popup');
    getFocused__element.parentNode.removeChild(getFocused__element); // send message to show snooze counter

    chrome.runtime.sendMessage({
      snoozeCounterPopup_askShowIframe: true
    });
  } // --- message that tell to show reminder pop up


  if (message.reminderPopup_show) {
    document.body.appendChild(reminderPopupFrame);
  } // --- when the snooze is over, recheck the URL, if still on distracting website then show iframe. Otherwise do nothing


  if (message.reminderPopup_snoozeOver) {
    chrome.runtime.sendMessage({
      reminderPopup_checkUrl: true
    });
  }
});