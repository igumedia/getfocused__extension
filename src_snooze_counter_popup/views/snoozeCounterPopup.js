const m = require("mithril")

module.exports = {
    snoozeCounter_totalCount: 0,
    snoozeCounter_totalDuration: 0,
    popUpDuration: 5,
    progressBarPrecentage: 100,
    intervalID: null,
    initCountdown: function () {
        let duration = this.popUpDuration * 1000
        let timeleft = this.popUpDuration * 1000

        this.intervalID = setInterval(() => {
            timeleft = timeleft - 10
            this.progressBarPrecentage = timeleft / duration * 100
            m.redraw()

            if (timeleft <= 0) {
                this.dismissPopup()
            }
        }, 10)
    },
    dismissPopup: function () {
        clearInterval(this.intervalID)
        this.progressBarPrecentage = 0
        this.sendMessageToRemoveIframe()
        m.redraw()
    },
    sendMessageToRemoveIframe: function () {
        chrome.runtime.sendMessage({ snoozeCounterPopup_askRemoveIframe: true })
    },
    oninit: function () {
        // store default value for reminder popup snooze duration websites if none found in localstorage
        chrome.storage.local.get('snoozeCounter_container', result => {
            let container = result.snoozeCounter_container;
            this.snoozeCounter_totalCount = container.snoozeCounter_totalCount;
            this.snoozeCounter_totalDuration = container.snoozeCounter_totalDuration.snoozeTotalDuration;
            
            // triger timer
            this.initCountdown()
        });

    },
    // oncreate: function () {
    //     // send message to the parent window of snooze counter popup height
    //     window.parent.postMessage({
    //         snoozeCounter_height: document.getElementById('getFocused__snoozeCounter-popup').offsetHeight
    //     }, "*")
    // },
    view: function (vnode) {
        return this.snoozeCounter_totalCount > 0 ? m('div.flex flex-col w-64 absolute right-0 cursor-pointer overflow-hidden', {
            id: 'snoozeCounerPopup',
            class: this.progressBarPrecentage <= 0 ? 'hidden' : null,
            onclick: function () {
                vnode.state.dismissPopup()
            },
            title: 'Click to dismiss'
        }, [
            m('div.w-full bg-gray-300 mb-2 rounded-lg', [
                m('div.bg-red-400 text-xs leading-none py-1 text-center text-white rounded-lg', {
                    style: 'width: ' + this.progressBarPrecentage + '%'
                }, '')
            ]),
            m('div.bg-gray-200 mb-8 shadow-xl rounded-lg  bg-opacity-100 p-2', [
                m('h1.text-center uppercase text-gray-800 tracking-widest font-body', 'Snooze Counter'),
                m('div.flex flex-row justify-center p-2 font-semibold font-sans semibold', [
                    m('div.text-6xl', this.snoozeCounter_totalCount),
                    m('div.text-6xl', 'X')

                ])
            ]),
            m('div.bg-gray-200 mb-8 shadow-xl rounded-lg bg-opacity-100 p-2', [
                m('h1.text-center uppercase text-gray-800 tracking-widest font-body ', 'Time you might waste'),
                m('div.flex flex-row justify-center p-2 font-semibold font-sans semibold', [
                    m('div.text-6xl', parseFloat((this.snoozeCounter_totalDuration).toFixed(2))),
                    m('div.text-base', 'min')
                ])
            ])

        ]) : null
    }
}
