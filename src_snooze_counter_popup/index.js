const m = require('mithril')
const snoozeCounterPopupView = require('./views/snoozeCounterPopup')

m.mount(document.body, snoozeCounterPopupView);
