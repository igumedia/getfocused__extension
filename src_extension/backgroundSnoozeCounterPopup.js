chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {

    // get tab id value
    let tabId = sender.tab.id

    // --- handler for snooze counter closed/time out
    if (request.snoozeCounterPopup_askRemoveIframe) {

        // send message to the coresponding tab to detach the iframe
        chrome.tabs.sendMessage(tabId, {
            snoozeCounterPopup_removeIframe: true
        })

    }

    // --- handler to show snooze counter, this event emitted by closed reminder popup
    if (request.snoozeCounterPopup_askShowIframe) {

        // send message to the coresponding tab to show snooze counter
        chrome.tabs.sendMessage(tabId, {
            snoozeCounterPopup_ShowIframe: true
        })
    }


    // --- handler to count snoozes
    if (request.snoozeCounter_store) {

        // get snooze counter from the localstorage
        function getSnoozeCounterContainer(type) {

            chrome.storage.local.get('snoozeCounter_container', function (result) {
                
                // parse the snooze count and duration value
                let snoozeCount = result.snoozeCounter_container.snoozeCounter_totalCount
                let snoozeDuration = result.snoozeCounter_container.snoozeCounter_totalDuration

                snoozeCount = snoozeCount + 1

                // calling function to process the total duration then store to local storage
                updateTotalSnoozeDuration({
                    snoozeCount: snoozeCount,
                    snoozeDuration: snoozeDuration,
                    snooze_type: type
                })

            })

        }

        // update snooze count values in local storage
        function updateTotalSnoozeDuration(container) {

            // get the snooze duration for the coresponding type
            chrome.storage.local.get('reminderPopupSnoozeDuration', function (result) {

                let now = new Date()
                // parse the snooze duration
                let duration = result.reminderPopupSnoozeDuration[container.snooze_type]

                // parse the snooze duration
                totalSnoozeCount = container.snoozeCount

                // parse the snooze duration from the argument
                totalSnoozeDuration = container.snoozeDuration

                // prepare the value contaiener
                let snoozeUntil
                let countedSnooze                

                // if the snooze duration is null (fresh startup) then immediatly assign value
                if (!totalSnoozeDuration.snoozeTotalDuration) {

                    snoozeUntil = now.setMinutes( now.getMinutes() + duration)

                    // counted snooze duration is according snooze type assigned by user
                    countedSnooze = duration

                } else {

                    // if previous snooze duration is not done but user make another snooze, then append the duration
                    if ( new Date(totalSnoozeDuration.snoozeUntil) > now) {

                        snoozeUntil = now.setMinutes( now.getMinutes() + duration)

                        // counted snooze is only the non used period by previos snooze
                        minuteDifference = Math.abs(new Date(snoozeUntil) - new Date(totalSnoozeDuration.snoozeUntil))/60000

                        countedSnooze = minuteDifference

                        // if current snooze duration is smaller than previous (already running) snooze then current snooze doesn't count
                        if ( new Date(totalSnoozeDuration.snoozeUntil) > new Date(snoozeUntil)) {

                            // snooze until is still the previos (already running snooze)
                            snoozeUntil = new Date(totalSnoozeDuration.snoozeUntil)

                            // current snooze doesn't count
                            countedSnooze = 0
                        }


                    } else { // if currently there is no running snooze

                        // snoozed until is now + duration
                        snoozeUntil = now.setMinutes( now.getMinutes() + duration)

                        // counted snooze duration is according snooze type assigned by user
                        countedSnooze = duration
                    }

                }

                // stora value to local storage
                chrome.storage.local.set({
                    snoozeCounter_container: {
                        snoozeCounter_totalCount: container.snoozeCount,
                        snoozeCounter_totalDuration: {
                            snoozeUntil: snoozeUntil,
                            snoozeTotalDuration: totalSnoozeDuration.snoozeTotalDuration + countedSnooze
                        }
                    }
                })


            })
        }

        getSnoozeCounterContainer(request.snooze_type)
    }



})