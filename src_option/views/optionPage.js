var m = require("mithril")
var optionModel = require('../model/Option').Option

module.exports = {
    oninit: function(){
        optionModel.getConfig()  
    },
    view: function(){
        return m('div.bg-gray-100 min-h-screen px-4 py-2 text-gray-800 text-base', [
            m('div.flex justify-between items-baseline', [
                m('div.p-2 bg-gray-100 flex flex-row justify-between items-baseline', [
                    m('div.bg-gray-100 font-sans p-1 text-xl text-teal-400 border-b-2 border-teal-400 inline-block rounded-lg cursor-pointer', 'Get Focused'),
                    m('span.text-sm ml-4 text-gray-500 self-start font-sans', optionModel.version)
                ]),
                // m('div', 'buy me a coffee')
            ]),
            m('div.flex flex-row', [
               
                m('form.mt-8 px-8 w-1/2',{
                    onsubmit: function(e){
                        e.preventDefault()
                        optionModel.saveReminderPopupConfiguration()
                    }
                },[
                    m('div.flex flex-row items-start leading-relaxed mb-8', [
                        m('div.w-1/2 p-2', [
                            m('div.font-bold text-teal-500 mb-2', 'Distracting websites'),
                            m('div.text-sm', 'Reminder popup will be shown when visiting these websites. Please separate each websites with new line.')
                        ]),
                        m('div.flex flex-row p-2', [
                            m('textarea.border border-gray-600 w-full p-2', {
                                cols: '25',
                                rows: '6',
                                required: true,
                                value: optionModel.getDistractingWebsites(),
                                oninput: function(e){
                                    optionModel.setDistractingWebsite(e.target.value)
                                }
                            })
                        ])
                    ]),
                    m('div.flex flex-row items-baseline leading-relaxed mb-8', [
                        m('div.w-1/2 p-2 flex-shrink-0', [
                            m('div.font-bold text-teal-500 mb-2', 'Snooze duration 1'),
                            m('div.text-sm', 'Hide reminder popup for x minutes')
                        ]),
                        m('div.flex flex-row p-2 w-full', [
                            m('input.w-16 border border-gray-600 text-right', {
                                required: true,
                                type: 'number',
                                min:'1',
                                value: optionModel.reminderPopupSnoozeDuration[1],
                                oninput: function(e){
                                    optionModel.setReminderPopupSnoozeDuration(1, parseInt(e.target.value))
                                }
                            }),
                            m('div.ml-2', 'Minutes')
                        ])
                    ]),
                    
                    m('div.flex flex-row items-baseline leading-relaxed mb-8', [
                        m('div.w-1/2 p-2 flex-shrink-0', [
                            m('div.font-bold text-teal-500 mb-2', 'Snooze duration 2'),
                            m('div.text-sm', 'Hide reminder popup for x minutes')
                        ]),
                        m('div.flex flex-row p-2 w-full', [
                            m('input.w-16 border border-gray-600 text-right', {
                                required: true,
                                type: 'number',
                                min:'1',
                                value: optionModel.reminderPopupSnoozeDuration[2],
                                oninput: function(e){
                                    optionModel.setReminderPopupSnoozeDuration(2, parseInt(e.target.value))
                                }
                            }),
                            m('div.ml-2', 'Minutes')
                        ])
                    ]),
                    m('div.flex flex-row items-baseline leading-relaxed mb-8', [
                        m('div.w-1/2 p-2', [
                            m('div.font-bold text-teal-500 mb-2', 'Snooze duration 3'),
                            m('div.text-sm', 'Hide reminder popup for x minutes')
                        ]),
                        m('div.flex flex-row p-2', [
                            m('input.w-16 border border-gray-600 text-right', {
                                required: true,
                                type: 'number',
                                min:'1',
                                value: optionModel.reminderPopupSnoozeDuration[3],
                                oninput: function(e){
                                    optionModel.setReminderPopupSnoozeDuration(3, parseInt(e.target.value))
                                }
                            }),
                            m('div.ml-2', 'Minutes')
                        ])
                    ]),
                    m('div.flex items-baseline mt-8' , [
                        m('button.text-white bg-teal-500 hover:bg-teal-600 py-2 px-5 rounded', 'Save'), 
                        m('div.ml-4 font-semibold text-teal-600',{
                            type: 'submit'
                        }, optionModel.statusMessage)
    
                    ])
                ]),
                m('div.mt-8 px-8 w-1/2', [
                    m('div', [
                        m('div.font-bold text-teal-500 mb-2', 'Have some feedback?'),
                        m('div.flex flex-row bg-white p-2 border rounded', [
                            m('div.text-6xl flex items-center justify-center rounded-full p-4 w-1/3 text-teal-500', '📧'),
                            m('form.w-2/3 pr-8',{
                                method: 'post',
                                action: 'https://api.formcubes.com/reg/v1/s/reg_3dxwfo10kdd95hx6'
                            },[
                                m('div.flex flex-col mb-4', [
                                    m('input.border rounded-sm p-1', {
                                        required: 'true',
                                        type: 'text',
                                        name: 'Name',
                                        placeholder: 'Name'
                                    })
                                ]),
                                m('div.flex flex-col mb-4', [
                                    m('input.border rounded-sm p-1', {
                                        placeholder: 'Email',
                                        required: 'true',
                                        type: 'email',
                                        name: 'Email',
                                    })
                                ]),
                                m('div.flex flex-col mb-4', [
                                    m('textarea.border rounded-sm p-1', {
                                        required: 'true',
                                        name: 'Message',
                                        rows: '6',
                                        placeholder: 'Report a bug, suggest a feature or ask a question...'
                                    })
                                ]),
                                m('button.text-white bg-indigo-500 hover:bg-indigo-600 py-2 px-5 rounded',{type: 'Submit'},'Send'),
                            ]),
                        ]),
                        m('div.p-4 bg-white mt-2 leading-relaxed text-xs', [
                            m('span', "This contact form using "),
                            m('a.text-teal-600 font-semibold', {href: 'https://formcubes.com', target: '_blank' }, 'formcubes.com '),
                            m('span', 'as the form backend. '),
                            m('span', "For disclosure I'm the founder. "),
                            m('span', "If you would like to know how your data will be used please review the "),
                            m('a.text-teal-600 font-semibold', {href: 'https://formcubes.com/privacy-policy/', target: '_blank' }, 'privacy policy '),
                            m('span', 'and '),
                            m('a.text-teal-600 font-semibold', {href: 'https://formcubes.com/what-we-collect/', target: '_blank' }, 'what we collect'),
                            m('br'),
                            m('br'),
                            m('span', "Or you can also contact me directly by email at "),
                            m('span.underline', "irfangumelar@gmail.com"),
                        ])

                    ])
                ])
            ])
        ])
    }
}