var m = require("mithril")

var Option = {
    version: '',
    statusMessage: false,
    distractingWebsites: [],
    distractingWebsitesInput: [],
    reminderPopupSnoozeDuration: [],
    getConfig: function(){
        
        var manifest = chrome.runtime.getManifest()
        Option.statusMessage = false
        chrome.storage.local.get(['version', 'distractingWebsites', 'reminderPopupSnoozeDuration'], function(result){
            Option.version = manifest.version
            Option.distractingWebsites = result.distractingWebsites
            Option.reminderPopupSnoozeDuration = result.reminderPopupSnoozeDuration
            m.redraw()
        })
    },
    getDistractingWebsites: function(){
        return Option.distractingWebsites.join('\n')
    },
    setDistractingWebsite: function(value){
        var array = value
        Option.distractingWebsites = array.split('\n')
    },
    setReminderPopupSnoozeDuration: function(type, value){
        Option.reminderPopupSnoozeDuration[type] = value
    },
    saveReminderPopupConfiguration: function(){

        Option.statusMessage = 'Loading...'

        var dw = Option.distractingWebsites.filter(el => el !== '')
        Option.distractingWebsites = dw

        chrome.storage.local.set({
            distractingWebsites: dw,
            reminderPopupSnoozeDuration: Option.reminderPopupSnoozeDuration
        }, function(){
            Option.statusMessage = 'Successfully saved!'
            m.redraw()
        })

        m.redraw()
    }

}

module.exports = {
    Option
}