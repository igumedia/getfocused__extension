var m = require("mithril")
var reminderModel = require('../model/Reminder').Reminder

module.exports = {
    oninit: function(){
        reminderModel.getSnoozeDuration()
        reminderModel.getTaskBag()
    },
    view: function(){
        return m('div.w-full h-full bg-gray-300 flex items-center justify-center', [
            m('div.w-3/4 h-screen font-body text-gray-800 p-5 leading-relaxed', [
                m('h1.text-3xl mt-10 mb-16 text-center', "This seems like a bad idea"),
                reminderModel.uncompletedTask > 0 ? m('div.text-2xl text-gray-700', "Let's tick these tasks off the list first") : null,
                m('ul.my-8 text-xl border-gray-300 mx-16 my-5 list-disc h-64 list-inside overflow-y-auto',
                    reminderModel.uncompletedTask === 0 ? m('div.text-gray-700 text-center mt-8', [
                        m('div', "You have not set your goals"),
                        m('br'),
                        m('div', 'Setting goals helps you to focus your efforts, allow you to measure your progress, and give you the motivation to strive forward')                    
                    ]) : null,
                    
                    reminderModel.taskBag.map(function(task){
                        if(task.completed === false ){
                            return m('li.mb-2', task.taskDesc)
                        }
                    })
                ),
                m('div.mt-10 flex flex row justify-between', [
                    m('button.rounded-full bg-teal-500 hover:bg-teal-600 text-white text-2xl py-2 px-5 duration-500 transition-opacity',{
                        onclick: function(){
                            reminderModel.closeTab()
                        }
                    },'Close this tab'),
                    m('button.border-b border-gray-700 text-gray-700 text-base py-2 px-5',{
                        onclick: function(){
                            reminderModel.snoozeTab(1)
                        }
                    }, reminderModel.snoozeDuration.length === '0' ? 'Loading' : 'Snooze for ' + reminderModel.snoozeDuration[1] + ' min'),
                    m('button.border-b border-gray-700 text-gray-700 text-base py-2 px-5',{
                        onclick: function(){
                            reminderModel.snoozeTab(2)
                        }
                    }, reminderModel.snoozeDuration.length === '0' ? 'Loading' : 'Snooze for ' + reminderModel.snoozeDuration[2] + ' min')
                ])

            ]),
            m('button.absolute bottom-0 right-0 p-1 text-gray-600', {
                onclick: function(){
                    // reminderModel.unlockSession()
                    reminderModel.snoozeTab(3)
                }
            }, reminderModel.snoozeDuration.length === '0' ? 'Loading' : 'Snooze for ' + reminderModel.snoozeDuration[3] + ' min (not recommended)')
        ])
    }
}
