var m = require('mithril')

var Reminder = {
    taskBag : [],
    taskBagLoading: true,
    snoozeDuration: [],
    uncompletedTask: 0,
    closeTab: function () {
        chrome.runtime.sendMessage({ reminderPopup_closeTab: true })
    },
    countUncompletedTask: function(){
        
        var counter = 0
        Reminder.uncompletedTask = counter++
        m.redraw()
    },
    getSnoozeDuration: function(){

        chrome.storage.local.get('reminderPopupSnoozeDuration', function(result){
            Reminder.snoozeDuration = result.reminderPopupSnoozeDuration
            m.redraw()
        })
    },
    getTaskBag: function(){

        // set loading state
        Reminder.taskBagLoading = true

        // set counter to zero
        Reminder.uncompletedTask = 0

        // get todos from taskbag
        chrome.storage.local.get('taskBag', function(result){
            Reminder.taskBag = result.taskBag
            Reminder.taskBagLoading = false
            
            var counter = 0

            if(result.taskBag){
                
                for (let i = 0; i < result.taskBag.length; i++) {
                    const e = result.taskBag[i];
    
                    if(e.completed === false){
                        counter = counter + 1
                    }
    
                    Reminder.uncompletedTask = counter
                    m.redraw()
                }

            }else{
                // fallback when taskBag is not set
                Reminder.taskBag = []
                Reminder.uncompletedTask = counter
                m.redraw()
            }
            

        });
    },
    snoozeTab: function (value) {

        // send message to remidner popup background
        chrome.runtime.sendMessage({
            reminderPopup_snooze: true,
            snooze_id: Date.now(),
            snooze_type: value
        })


        // send message to counter background
        chrome.runtime.sendMessage({
            snoozeCounter_store: true,
            snooze_type: value
        })

    },
    unlockSession: function(){

        chrome.runtime.sendMessage({
            reminderPopup_unlockSession: true
        })

    }

}

module.exports = {
    Reminder
}